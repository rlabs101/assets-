var ForcePortrait = pc.createScript('forcePortrait');

// initialize code called once per entity
ForcePortrait.prototype.initialize = function() {
    
};

// update code called every frame
ForcePortrait.prototype.update = function(dt) {
    
};

// swap method called for script hot-reloading
// inherit your script state here
// ForcePortrait.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/