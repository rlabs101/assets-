var ArrowControl = pc.createScript('arrowControl');

// initialize code called once per entity
ArrowControl.prototype.initialize = function() {
    
};

// update code called every frame
ArrowControl.prototype.update = function(dt) {
    
};

// swap method called for script hot-reloading
// inherit your script state here
// ArrowControl.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/